﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ProSportsClubApp
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private bool mediaIsPlaying = false;
        private bool userIsDraggingSlider = false;
        private int oldPosition = -1;

        private Rectangle previousRectangle;
        private TextBlock previousTextBlock;

        private Thickness Thickness15 = new Thickness(15);
        private Thickness Thickness35 = new Thickness(0, 35, 0, 0);

        private SolidColorBrush OrangeBrush = new SolidColorBrush();
        private SolidColorBrush GrayBrush = new SolidColorBrush();

        private FontFamily TradeGothic = new FontFamily("Trade Gothic LT Std");
        private FontFamily TradeGothicLight = new FontFamily("Trade Gothic LT Std Light");
        private bool IsFullscreen;
        private Size _previousVideoContainerSize = new Size();
        private object oldContent;
        private TimeSpan playbackPosition;

        private TimeSpan fitnessStart = TimeSpan.FromMilliseconds(18100);
        private TimeSpan sportsMedStart = TimeSpan.FromMilliseconds(31400);
        private TimeSpan wellnessStart = TimeSpan.FromMilliseconds(36200);
        private TimeSpan sportsStart = TimeSpan.FromMilliseconds(42030);
        private TimeSpan familyStart = TimeSpan.FromMilliseconds(71000);
        private TimeSpan spaStart = TimeSpan.FromMilliseconds(78200);
        private TimeSpan amenitiesStart = TimeSpan.FromMilliseconds(94000);
        private TimeSpan amenitiesEnd = TimeSpan.FromMilliseconds(113000);
        private String Learn_More_Url;

        public Window1()
        {
            InitializeComponent();

            SetContent(ProSportsClubApp.Properties.Resources.pro_sports_club_title, ProSportsClubApp.Properties.Resources.pro_sports_club_text, null);
            Thumbnail_Caption.Text = ProSportsClubApp.Properties.Resources.thumbnail_caption;

            OrangeBrush.Color = Color.FromRgb(255, 128, 0);
            OrangeBrush.Opacity = 0.45;
            GrayBrush.Color = Color.FromRgb(40, 50, 46);
            GrayBrush.Opacity = 0.6;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += timer_Tick;
            timer.Start();

            Video.ScrubbingEnabled = true;
            Video.Play();
            Video.Pause();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((Video.Source != null) && (Video.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = Video.NaturalDuration.TimeSpan.TotalMilliseconds;
                sliProgress.Value = Video.Position.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Launch web browser to pro sports club web site when link is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onViewWebsiteClicked(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.proclub.com");
        }

        /// <summary>
        /// Handle mouse down event on video window. If video is playing, then pause it; if video is paused, then start playing it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onVideoPlayerClicked(object sender, MouseButtonEventArgs e)
        {
            if (mediaIsPlaying)
            {
                StopMedia();
            }
            else
            {
                StartMedia();
            }
        }

        private void StopMedia()
        {
            if (mediaIsPlaying)
            {
                PlayButton.Source = new BitmapImage(new Uri("Assets/Images/media-playback-start-6.png", UriKind.Relative));
                mediaIsPlaying = false;
                Video.Pause();
                playbackPosition = Video.Position;
            }
        }

        private void StartMedia()
        {
            Splash_Logo.Visibility = Visibility.Hidden;
            if (!mediaIsPlaying)
            {
                PlayButton.Source = new BitmapImage(new Uri("Assets/Images/media-playback-pause-6.png", UriKind.Relative));
                mediaIsPlaying = true;
                Video.Play();
                Video.Position = playbackPosition;
            }
        }

        private void onFitnessClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.fitness_title, ProSportsClubApp.Properties.Resources.fitness_text, 
                ProSportsClubApp.Properties.Resources.fitness_url);
            // Overloaded constructor takes the arguments days, hours, minutes, seconds, milliseconds. 
            // Create a TimeSpan with milliseconds equal to the slider value.
            StartMedia();
            Video.Position = fitnessStart;

            Highlight(RecFitness1, imgFitnessText);
        }

        private void onSportsMedClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.sportsmed_title, ProSportsClubApp.Properties.Resources.sportsmed_text, 
                ProSportsClubApp.Properties.Resources.sportsmed_url);
            StartMedia();
            Video.Position = sportsMedStart;

            Highlight(RecSportsMed, imgSportsMedText);
        }

        private void onWellnessClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.wellness_title, ProSportsClubApp.Properties.Resources.wellness_text,
                ProSportsClubApp.Properties.Resources.wellness_url);
            StartMedia();
            Video.Position = wellnessStart;

            Highlight(RecWellness, imgWellnessText);
        }

        private void onSportsClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.sports_title, ProSportsClubApp.Properties.Resources.sports_text,
                ProSportsClubApp.Properties.Resources.sports_url);
            StartMedia();
            Video.Position = sportsStart;

            Highlight(RecSports, imgSportsText);
        }

        private void onFamilyClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.family_title, ProSportsClubApp.Properties.Resources.family_text,
                ProSportsClubApp.Properties.Resources.family_url);
            StartMedia();
            Video.Position = familyStart;

            Highlight(RecFamily, imgFamilyText);
        }

        private void onSpaClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.spa_title, ProSportsClubApp.Properties.Resources.spa_text,
                ProSportsClubApp.Properties.Resources.spa_url);
            StartMedia();
            Video.Position = spaStart;

            Highlight(RecSpa, imgSpaText);
        }

        private void onAmenitiesClicked(object sender, MouseButtonEventArgs e)
        {
            SetContent(ProSportsClubApp.Properties.Resources.amenities_title, ProSportsClubApp.Properties.Resources.amenities_text,
                ProSportsClubApp.Properties.Resources.amenities_url);
            StartMedia();
            Video.Position = amenitiesStart;

            Highlight(RecAmenities, imgAmenitiesText);
        }

        private void SetContent(string title, string text, String url)
        {
            Title_For_Video.Text = title;
            Text_For_Video.Text = text;
            if (url != null)
            {
                Learn_More.Visibility = Visibility.Visible;
                Learn_More_Url = url;
            }
            else
            {
                Learn_More.Visibility = Visibility.Collapsed;
                Learn_More_Url = "http://www.proclub.com";
            }
        }

        private void Highlight(Rectangle rectangle, TextBlock textBlock)
        {
            if (previousRectangle == rectangle)
                return;

            UnHighlight();
            rectangle.Height = 95;
            rectangle.Fill = OrangeBrush;

            textBlock.FontFamily = TradeGothic;
            textBlock.FontWeight = FontWeights.Bold;
            textBlock.Height = 95;
            textBlock.Padding = Thickness35;

            previousRectangle = rectangle;
            previousTextBlock = textBlock;
        }

        private void UnHighlight()
        {
            if (previousRectangle != null)
            {
                previousRectangle.Height = 55.8125;
                previousRectangle.Fill = GrayBrush;
                previousRectangle = null;
            }

            if (previousTextBlock != null)
            {
                previousTextBlock.FontFamily = TradeGothicLight;
                previousTextBlock.FontWeight = FontWeights.Normal;
                previousTextBlock.Height = 55.8125;
                previousTextBlock.Padding = Thickness15;
                previousTextBlock = null;
            }
        }

        private void Left_Arrow_Click(object sender, MouseButtonEventArgs e)
        {
            Thumbnails.LineLeft();
        }

        private void Right_Arrow_Click(object sender, MouseButtonEventArgs e)
        {
            Thumbnails.LineRight();
        }

        private void Video_MouseEnter(object sender, MouseEventArgs e)
        {
            StatusBar.Visibility = Visibility.Visible;
        }

        private void Video_MouseLeave(object sender, MouseEventArgs e)
        {
            StatusBar.Visibility = Visibility.Collapsed;
            e.Handled = true;
        }

        private void sliProgress_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            Video.Position = TimeSpan.FromMilliseconds(sliProgress.Value);
            playbackPosition = Video.Position;
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromMilliseconds(sliProgress.Value).ToString(@"hh\:mm\:ss");

            int position = -1;

            if (sliProgress.Value < fitnessStart.TotalMilliseconds || sliProgress.Value >= amenitiesEnd.TotalMilliseconds)
            {
                UnHighlight();
                SetContent(ProSportsClubApp.Properties.Resources.pro_sports_club_title, ProSportsClubApp.Properties.Resources.pro_sports_club_text, null);
            }
            if (sliProgress.Value >= fitnessStart.TotalMilliseconds && sliProgress.Value < sportsMedStart.TotalMilliseconds)
            {
                Highlight(RecFitness1, imgFitnessText);
                SetContent(ProSportsClubApp.Properties.Resources.fitness_title, ProSportsClubApp.Properties.Resources.fitness_text,
                    ProSportsClubApp.Properties.Resources.fitness_url);
                position = 0;
            }
            if (sliProgress.Value >= sportsMedStart.TotalMilliseconds && sliProgress.Value < wellnessStart.TotalMilliseconds)
            {
                Highlight(RecSportsMed, imgSportsMedText);
                SetContent(ProSportsClubApp.Properties.Resources.sportsmed_title, ProSportsClubApp.Properties.Resources.sportsmed_text,
                    ProSportsClubApp.Properties.Resources.sportsmed_url);
                position = 1;
            }
            if (sliProgress.Value >= wellnessStart.TotalMilliseconds && sliProgress.Value < sportsStart.TotalMilliseconds)
            {
                Highlight(RecWellness, imgWellnessText);
                SetContent(ProSportsClubApp.Properties.Resources.wellness_title, ProSportsClubApp.Properties.Resources.wellness_text,
                    ProSportsClubApp.Properties.Resources.wellness_url);
                position = 2;
            }
            if (sliProgress.Value >= sportsStart.TotalMilliseconds && sliProgress.Value < familyStart.TotalMilliseconds)
            {
                Highlight(RecSports, imgSportsText);
                SetContent(ProSportsClubApp.Properties.Resources.sports_title, ProSportsClubApp.Properties.Resources.sports_text,
                    ProSportsClubApp.Properties.Resources.sports_url);
                position = 3;
            }
            if (sliProgress.Value >= familyStart.TotalMilliseconds && sliProgress.Value < spaStart.TotalMilliseconds)
            {
                Highlight(RecFamily, imgFamilyText);
                SetContent(ProSportsClubApp.Properties.Resources.family_title, ProSportsClubApp.Properties.Resources.family_text,
                    ProSportsClubApp.Properties.Resources.family_url);
                position = 4;
            }
            if (sliProgress.Value >= spaStart.TotalMilliseconds && sliProgress.Value < amenitiesStart.TotalMilliseconds)
            {
                Highlight(RecSpa, imgSpaText);
                SetContent(ProSportsClubApp.Properties.Resources.spa_title, ProSportsClubApp.Properties.Resources.spa_text,
                    ProSportsClubApp.Properties.Resources.spa_url);
                position = 5;
            }
            if (sliProgress.Value >= amenitiesStart.TotalMilliseconds && sliProgress.Value < amenitiesEnd.TotalMilliseconds)
            {
                Highlight(RecAmenities, imgAmenitiesText);
                SetContent(ProSportsClubApp.Properties.Resources.amenities_title, ProSportsClubApp.Properties.Resources.amenities_text,
                    ProSportsClubApp.Properties.Resources.amenities_url);
                position = 6;
            }

            if (position > -1 && oldPosition != position)
            {
                // position of your visual inside the scrollviewer    
                GeneralTransform childTransform = previousRectangle.TransformToAncestor(Thumbnails);
                Rect rectangle = childTransform.TransformBounds(new Rect(new Point(0, 0), previousRectangle.RenderSize));

                //Check if the elements Rect intersects with that of the scrollviewer's
                Rect result = Rect.Intersect(new Rect(new Point(0, 0), Thumbnails.RenderSize), rectangle);
                //if result is Empty then the element is not in view
                if (result.Width < rectangle.Width)
                {
                    Thumbnails.ScrollToHorizontalOffset(position);
                }

                oldPosition = position;
            }
        }

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Video.Volume += (e.Delta > 0) ? 0.1 : -0.1;
        }

        private void pbVolume_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ProgressBar volume = sender as ProgressBar;
            Point p = e.GetPosition(volume);
            double x = p.X / 50.0;
            volume.Value = x;
        }

        private void FullScreen_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsFullscreen = !IsFullscreen;
            StatusBar.Visibility = Visibility.Collapsed;
            _previousVideoContainerSize.Width = Video.Width;
            _previousVideoContainerSize.Height = Video.Height;
            playbackPosition = Video.Position;

            oldContent = this.Content;
            MediaGrid.Children.Remove(Video);
            this.Content = Video;

            this.Background = new SolidColorBrush(Colors.Black);
            this.WindowStyle = WindowStyle.None;
            this.WindowState = WindowState.Maximized;
            Console.WriteLine("Window height = " + Window_Main2.ActualHeight);
            Console.WriteLine("Window width = " + Window_Main2.ActualWidth);

            Video.Position = playbackPosition;
            Video.Height = this.ActualHeight;
            Video.Width = this.ActualWidth;
            Video.Stretch = Stretch.Fill;
        }

        private void sliProgress_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!userIsDraggingSlider)
            {
                double x = e.GetPosition(sliProgress).X;
                double ratio = x / sliProgress.ActualWidth;
                double newPosition = Video.NaturalDuration.TimeSpan.TotalMilliseconds * ratio;
                Video.Position = TimeSpan.FromMilliseconds(newPosition);
                playbackPosition = TimeSpan.FromMilliseconds(newPosition);
            }
        }

        private void Video_MediaEnded(object sender, RoutedEventArgs e)
        {
            playbackPosition = TimeSpan.FromSeconds(0);
            Video.Position = playbackPosition;
            StopMedia();
            Splash_Logo.Visibility = Visibility.Visible;
        }

        private void Window_Main2_KeyUp(object sender, KeyEventArgs e)
        {
            if (IsFullscreen)
            {
                if (e.Key == Key.Escape)
                {
                    playbackPosition = Video.Position;
                    this.Content = oldContent;

                    Video.Height = _previousVideoContainerSize.Height;
                    Video.Width = _previousVideoContainerSize.Width;

                    MediaGrid.Children.Add(Video);
                    StatusBar.Visibility = Visibility.Visible;
                    int videoZIndex = Int32.Parse(Video.GetValue(Panel.ZIndexProperty).ToString());
                    StatusBar.SetValue(Panel.ZIndexProperty, videoZIndex + 1);

                    this.Background = new SolidColorBrush(Colors.White);
                    this.WindowStyle = WindowStyle.SingleBorderWindow;
                    this.WindowState = WindowState.Normal;
                    IsFullscreen = !IsFullscreen;
                    Video.Position = playbackPosition;
                }
            }
        }

        private void Learn_More_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start(Learn_More_Url);
        }
    }
}
