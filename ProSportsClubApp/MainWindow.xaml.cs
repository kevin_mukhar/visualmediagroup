﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Resources;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;

namespace ProSportsClubApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Boolean mediaIsPlaying = false;
        Boolean isFullscreenToggle = false;
        Grid currentGrid = null;
        Rectangle currentRectangle = null;
        TextBlock currentText = null;
        private Size _previousVideoContainerSize = new Size();

        public MainWindow()
        {
            InitializeComponent();

            Title_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_title;
            Text_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_text;
            View_Our_Website.Text = ProSportsClubApp.Properties.Resources.View_Our_Website;
            Learn_More.Text = ProSportsClubApp.Properties.Resources.learn_more;
        }

        /// <summary>
        /// Handle mouse down event on video window. If video is playing, then pause it; if video is paused, then start playing it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onVideoPlayerClicked(object sender, MouseButtonEventArgs e)
        {
            splash_image.Visibility = Visibility.Hidden;
            if (video.Source == null)
            {
                Uri uri = new Uri(@"Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4", UriKind.Relative);
                video.Source = uri;
                video.Pause();
                currentGrid = Grid1;
                currentRectangle = Rectangle1;
                currentText = imgFitnessText;
                Highlight();
            }
            if (mediaIsPlaying)
            {
                video.Pause();
                mediaIsPlaying = false;
                Play_Button.Visibility = Visibility.Visible;
                Pause_Button.Visibility = Visibility.Collapsed;
            }
            else
            {
                video.Play();
                mediaIsPlaying = true;
                Play_Button.Visibility = Visibility.Collapsed;
                Pause_Button.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Launch web browser to pro sports club web site when link is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onViewWebsiteClicked(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.proclub.com");
        }

        private void onImgClicked(object sender, MouseButtonEventArgs e)
        {
            splash_image.Visibility = Visibility.Hidden;
            FrameworkElement frameworkElement = sender as FrameworkElement;
            String uid = frameworkElement.Uid;
            String currentSource = "";
            if (video.Source != null)
            {
                currentSource = video.Source.ToString();
            }
            String requestedSource = "";

            switch (uid)
            {
                case "imgFitness":
                    requestedSource = "Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_text;
                    UnHighlight();
                    currentGrid = Grid1;
                    currentRectangle = Rectangle1;
                    currentText = imgFitnessText;
                    Highlight();
                    break;
                case "imgAmenities":
                    requestedSource = "Assets/Videos/PRO Sports Club Shiatsu (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.amenities_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.amenities_text;
                    UnHighlight();
                    currentGrid = Grid2;
                    currentRectangle = Rectangle2;
                    currentText = imgAmenitiesText;
                    Highlight();
                    break;
                case "imgSpa":
                    requestedSource = "Assets/Videos/PRO Sports Club Spa (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.spa_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.spa_text;
                    UnHighlight();
                    currentGrid = Grid3;
                    currentRectangle = Rectangle3;
                    currentText = imgSpaText;
                    Highlight();
                    break;
                case "imgSportsMed":
                    requestedSource = "Assets/Videos/PRO Sports Club Podiatry (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.sportsmed_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.sportsmed_text;
                    UnHighlight();
                    currentGrid = Grid4;
                    currentRectangle = Rectangle4;
                    currentText = imgSportsMedText;
                    Highlight();
                    break;
                case "imgWellness":
                    requestedSource = "Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.wellness_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.wellness_text;
                    UnHighlight();
                    currentGrid = Grid5;
                    currentRectangle = Rectangle5;
                    currentText = imgWellnessText;
                    Highlight();
                    break;
                case "imgFamily":
                    requestedSource = "Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4";
                    Title_For_Video.Text = ProSportsClubApp.Properties.Resources.family_title;
                    Text_For_Video.Text = ProSportsClubApp.Properties.Resources.family_text;
                    UnHighlight();
                    currentGrid = Grid6;
                    currentRectangle = Rectangle6;
                    currentText = imgFamilyText;
                    Highlight();
                    break;
            }
            if (!requestedSource.Equals(currentSource))
            {
                PlayVideo(requestedSource);
            }
        }

        private void Highlight()
        {
            if (currentGrid == null)
                return;
            currentGrid.Height = 80;
            Canvas.SetTop(currentGrid, 601);
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromRgb(255, 128, 0);
            mySolidColorBrush.Opacity = 0.45;
            currentRectangle.Fill = mySolidColorBrush;
            currentText.FontWeight = FontWeights.Bold;
        }

        private void UnHighlight()
        {
            if (currentGrid == null)
                return;
            currentGrid.Height = 40;
            Canvas.SetTop(currentGrid, 641);
            SolidColorBrush mySolidColorBrush = new SolidColorBrush();
            mySolidColorBrush.Color = Color.FromRgb(40, 50, 46);
            mySolidColorBrush.Opacity = 0.6;
            currentRectangle.Fill = mySolidColorBrush;
            currentText.FontWeight = FontWeights.Light;
        }

        private void video_MediaEnded(object sender, RoutedEventArgs e)
        {
            String currentSource = video.Source.ToString();
            String nextSource = "";
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Shiatsu (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.amenities_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.amenities_text;
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Shiatsu (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Spa (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.spa_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.spa_text;
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Spa (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Podiatry (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.sportsmed_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.sportsmed_text;
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Podiatry (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.wellness_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.wellness_text;
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.family_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.family_text;
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4"))
            {
                nextSource = "Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4";
                Title_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_title;
                Text_For_Video.Text = ProSportsClubApp.Properties.Resources.fitness_text;
            }

            PlayVideo(nextSource);
        }

        private void PlayVideo(String requestedSource)
        {
            Uri uri = new Uri(requestedSource, UriKind.Relative);
            setNewVideoSource(uri);
        }

        private void setNewVideoSource(Uri uri)
        {
            video.Source = uri;
            video.Play();
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void onVolumeClicked(object sender, MouseButtonEventArgs e)
        {
        }

        private void VideoContainer_KeyUp(object sender, KeyEventArgs e)
        {
            if (IsFullscreen && e.Key == Key.Escape)
            {
                FullscreenToggle();
            }

            e.Handled = true;
        }

        private void videoElement_MediaOpened(object sender, RoutedEventArgs e)
        {

        }

        private void onFullScreenClicked(object sender, MouseButtonEventArgs e)
        {
            //FullscreenToggle();
        }

        public Boolean IsFullscreen
        {
            get { return isFullscreenToggle; }
            set { isFullscreenToggle = value; }
        }

        private void FullscreenToggle()
        {
            this.IsFullscreen = !this.IsFullscreen;

            if (this.IsFullscreen)
            {
                TransportControlsPanel.Visibility = Visibility.Collapsed;

                _previousVideoContainerSize.Width = videoContainer.ActualWidth;
                _previousVideoContainerSize.Height = videoContainer.ActualHeight;

                videoContainer.Width =  System.Windows.SystemParameters.WorkArea.Width;
                videoContainer.Height = System.Windows.SystemParameters.WorkArea.Height;
                video.Width = System.Windows.SystemParameters.WorkArea.Width;
                video.Height = System.Windows.SystemParameters.WorkArea.Height;
            }
            else
            {
                TransportControlsPanel.Visibility = Visibility.Visible;

                videoContainer.Width = _previousVideoContainerSize.Width;
                videoContainer.Height = _previousVideoContainerSize.Height;
                video.Width = _previousVideoContainerSize.Width;
                video.Height = _previousVideoContainerSize.Height;
            }
        }
    }
}
