﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ProSportsClubWin8
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Boolean mediaIsPlaying = false;

        public MainPage()
        {
            this.InitializeComponent();
            Uri uri = new Uri("ms-appx-web:///Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4", UriKind.Absolute);
            video.Source = uri;
            video.Pause();
        }

        /// <summary>
        /// Handle mouse down event on video window. If video is playing, then pause it; if video is paused, then start playing it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onVideoPlayerClicked(object sender, TappedRoutedEventArgs e)
        {
            splash_image.Visibility = Visibility.Collapsed;
            if (mediaIsPlaying)
            {
                video.Pause();
                mediaIsPlaying = false;
            }
            else
            {
                video.Play();
                mediaIsPlaying = true;
            }
        }

        /// <summary>
        /// Launch web browser to pro sports club web site when link is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onViewWebsiteClicked(object sender, TappedRoutedEventArgs e)
        {
            //System.Diagnostics.Process.Start("http://www.proclub.com");
        }

        private void onImgClicked(object sender, TappedRoutedEventArgs e)
        {
            splash_image.Visibility = Visibility.Collapsed;
            Image img = sender as Image;
            System.String currentSource = video.Source.ToString();
            System.String requestedSource = "";

            if (img.Name.Equals("imgFitness"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4";
            }
            if (img.Name.Equals("imgAmenities"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Shiatsu (Low).mp4";
            }
            if (img.Name.Equals("imgSpa"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Spa (Low).mp4";
            }
            if (img.Name.Equals("imgSportsMed"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Podiatry (Low).mp4";
            }
            if (img.Name.Equals("imgWellness"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4";
            }
            if (img.Name.Equals("imgFamily"))
            {
                requestedSource = "Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4";
            }

            if (!requestedSource.Equals(currentSource))
            {
                System.Uri uri = new System.Uri("ms-appx-web:///" + requestedSource, UriKind.Absolute);
                setNewVideoSource(uri);
            }
        }

        private void video_MediaEnded(object sender, RoutedEventArgs e)
        {
            System.String currentSource = video.Source.ToString();
            System.String nextSource = "";
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Shiatsu (Low).mp4";
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Shiatsu (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Spa (Low).mp4";
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Spa (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Podiatry (Low).mp4";
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Podiatry (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4";
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Physical Therapy (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4";
            }
            if (currentSource.Equals("Assets/Videos/PRO Sports Club Family Fun Commercial (Low).mp4"))
            {
                nextSource = "ms-appx-web:///Assets/Videos/PRO Sports Club Personal Training Commercial (Low).mp4";
            }

            Uri uri = new Uri(nextSource, UriKind.Absolute);
            setNewVideoSource(uri);
        }

        private void setNewVideoSource(System.Uri uri)
        {
            //video.Pause();
            video.Source = uri;
            video.Play();
        }
    }
}
